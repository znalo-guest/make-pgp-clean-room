PGP/PKI Clean Room Live CD
==========================

Requirements
------------

 - Three external storage devices with at least 5 MB of storage
	- Such as USB flash drives or SD cards
	- They will be wiped during the export step so ensure any of the data currently on them is backed up
	- Two will be for master key backups and need to be stored in a safe place, preferably two different safe places
	- The last will be for public key export and can be reused for other things after your new key is imported to your computer
 - One i686-compatible PC
	- Most modern computers and even many older computers should work
	- You need at least 1 GB of RAM

 - One blank cd or blank USB drive
	- To run the installer off of
	- Could be reused as a backup drive after the system is booted

 - A PC running Debian
	- Only required to generate the install CD from source
	- Could be done from another Debian live CD as well

Latest Builds
-------------

The latest builds are available on [Thomas Levine's server](http://pgpcr.tookmund.com)
or [Google Drive](https://drive.google.com/open?id=12C4LbiZ8HuZFfPdZzRm561Wyg7TJiEvj)

There should be `.sig` files for each build, signed with [my GPG key](https://tookmund.com/assets/3F90059E1AFDDD53.asc).

Running
-------
 - Get my GPG key for verifying the signature:

 ```shell
 gpg --recv-keys D3EAC374AC2B30DDC1B30A7F3F90059E1AFDDD53
 ```
 - Download the latest build, along with its `.sig` file:

 E.g. for version 1.0:

 ```shell
 wget http://pgpcr.tookmund.com/pgpcr-v1.0.iso
 wget http://pgpcr.tookmund.com/pgpcr-v1.0.iso.sig
 ```
 - Verify the signature with gpg, using the key you downloaded earlier:

 ```shell
 gpg --verify pgpcr-v1.0.iso.sig
 ```
 - Burn the ISO image to a CD or USB
	- Most systems should have a program to do this fairly easily for a CD.
	- See the [Debian installation manual](https://www.debian.org/releases/stretch/i386/ch04s03.html.en) for tips on how to write the ISO to a USB flash drive
	- The image is a hybrid ISO file so just copying it directly to the USB stick (NOT a parition, but directly to the device) should work

	E.g. assuming your USB stick is `/dev/sdX`:

	```shell
	cp pgpcr-v1.0.iso /dev/sdX
	sync
	```
 - Boot from this device
	- Again, see the [Debian installation manual](https://www.debian.org/releases/stretch/i386/ch03s06.html.en#boot-dev-select) for how to select the correct boot device.
 - Follow the instructions in the PGP Clean Room application that appears once the system boots

Expert Mode
-----------
By default the application makes many decisions for you in an attempt to simplify the process. If you would rather it did not, you can enable expert mode in the advanced options menu

Building from source
-------------------

Clone this repository:

```shell
git clone https://salsa.debian.org/tookmund-guest/make-pgp-clean-room.git
cd make-pgp-clean-room
git submodule init
```

Install its dependencies:

* build-essential
* live-build
* debootstrap
* rsync
* sudo
* squashfs-tools

You'll also need the [application's dependencies](https://salsa.debian.org/tookmund-guest/pgpcr/blob/master/README.md#dependencies)

Run the build script:
```shell
./scripts/make-pgp-clean-room
```
